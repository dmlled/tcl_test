# http://wiki.tcl.tk/14465
package require tcltest 2.0
namespace import -force ::tcltest::*

# https://wuhrr.wordpress.com/2013/09/13/tcltest-part-9-provides-exit-code/
proc tcltest::cleanupTestsHook {} {
    variable numTests
    set ::exitCode [expr {$numTests(Failed) > 0}]
}

test intro-1 {This is as simple as it gets.} {
	expr 3 * 5
} 15

test intro-2 {This illustrates how something goes wrong.} {
	expr 21 * 2
} 33


test intro-3 {It's very important to validate responses when
				things go wrong.} -body {
	expr 3 / 0
} -returnCodes error -result {divide by zero}

cleanupTests

exit $::exitCode
